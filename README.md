# README #

Only other somewhat similar tool I found was DEEP (2006), but that was focused more on malware propagation and not C&C emulation

 
Threat Emulator & Evaluation Toolset (TEET)
A series of scripts which model APT exfill behavior.  Designed to be used by Red Cell teams to test their companies CIRT.
Future versions will include UID's so that CIRT can sort the "testing" traffic from the real thing if that happens.

Current Modules:

ICMP ping exfill
HTTP exfill

Future scripts in the works:

DNS --> IPV6 DNS Requests with info in plain text and sent to rogue dns server
Blender --> Fake HTTP traffic that only sends during web browsing.
Debating making it send to the same domain, but placing a UID in it...
This would "mimic" an advanced attack where your ISP infrastructure was PWND and they could route as they see fit.



### How do I get set up? ###

* Programmed in Python 2.7
* Dependencies are straightforward, you do need to install Impacket


### Who do I talk to? ###

* keyzer@penetrate.io
* penetrate.io